require 'test_helper'
require Rails.root.join('lib/clean_prereq').to_s

class CleanPrereqTest < ActiveSupport::TestCase
  test 'levels' do
    assert_equal( [{ level: 11 }], CleanPrereq.new('11th level').clean )
    assert_equal( [{ level: 21 }], CleanPrereq.new('21st level').clean )
  end

  test 'classes' do

    assert_equal( [{ class: 'avenger' }], CleanPrereq.new('Avenger').clean )
    assert_equal( [{ class: 'paladin' }], CleanPrereq.new('paladin class').clean )
    assert_equal( [{ class: 'barbarian' }], CleanPrereq.new('barbarian').clean )
    assert_equal( [{ class: 'rogue', race: 'drow' }], CleanPrereq.new('drow rogue').clean )
    assert_equal( [{ class: 'ranger', race: 'halfling' }], CleanPrereq.new('Halfling ranger').clean )
    assert_equal( [[{race: 'halfling', class: 'rogue'}, {race: 'halfling', class:'paladin'}]], CleanPrereq.new('Halfling rogue or halfling paladin').clean )
    # assert_equal( { level: 11, 'Cha' => 15], power: ['lay on hands', 'virtues's touch'] }, CleanPrereq.new('11th level; Cha 15; paladin; \
    # # lay on hands or virtue's touch power'').clean )
    # assert_equal [['level', '11' ], ['Cha 15', '1'], ['power', 'ardent lay on hands, virtues's touch']], CleanPrereq.new('11th level; paladin; ardent vow, \
    # lay on hands, or virtue's touch power').clean
    #').clean
  end

  test 'attributes' do
    assert_equal( [{ level: 11, 'cha' => 15, class:'paladin'}], CleanPrereq.new('11th level; Cha 15; paladin').clean )
    assert_equal( [{ level: 11, 'cha' => 15, 'wis' => 13}], CleanPrereq.new('11th level, Cha 15, wis 13').clean )
    assert_equal( [{ level: 11}, [{'cha' => 15}, {'wis' => 13}]], CleanPrereq.new('11th level; Cha 15 or wis 13').clean )
  end

  test 'power sources' do
    assert_equal( [{ 'dex' => 13, power_source: 'martial'}], CleanPrereq.new('Dex 13, any martial class').clean )
    assert_equal( [{ power_source: 'martial'}, [{'dex' => 13}, {race: 'halfling'}]], CleanPrereq.new('Dex 13 or halfling race; any martial class').clean )
  end

  test 'extra prereqs' do
    assert_equal( [{ level: 11, race: 'eladrin', class: 'fighter'}], CleanPrereq.new('11th level, eladrin, fey step racial power, fighter').clean )
    assert_equal( [{ race: 'dragonborn', class: 'warlord'}], CleanPrereq.new('Dragonborn, warlord, Bravura Presence class feature').clean )
  end

end
