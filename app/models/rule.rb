class Rule < ActiveRecord::Base
  include PgSearch
  pg_search_scope :search_by_content, :against => :content

  has_many :prerequisites

  def self.search(search_params)
    if search_params.blank?
      @rules = Rule.first(10)
    else
      @rules = Rule.search_by_content(search_params)
    end
  end
end
