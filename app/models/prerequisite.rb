class Prerequisite #< ActiveRecord::Base
  CLASSES = ["ardent", "artificer", "assasin", "avenger", "barbarian", "bard", "battlemind", "berserker", "cleric", \
    "fighter", "invoker", "monk", "paladin", "psion", "ranger", "rogue", "runepriest", "seeker", "shaman", \
    "sorcerer", "swordmage", "warden", "warlock", "warlord", "wizard", "vampire", "druid", "hunter", "scout"]

  POWER_SOURCES = { "divine"  => ["avenger", "cleric", "paladin", "invoker", "runepriest"],
                    "martial" => ["berserker", "bard", "fighter", "ranger", "rogue", "warlord"],
                    "primal"  => ["barbarian", "druid", "hunter", "scout", "seeker", "shaman", "warden"],
                    "arcane"  => ["artificer", "bard", "sorcerer", "swordmage", "warlock", "wizard"],
                    "psionic" => ["ardent", "battlemind", "monk", "psion"],
                    "shadow"  => ["assasin", "vampire"] }

  RACES = ["bladling", "changeling", "deva", "dragonborn", "drow", "dwarf", "eladrin", "elf", "genasi", \
    "githzerai", "gnome", "goliath", "half-elf", "half-orc", "halfling", "hamadryad", "hengeyokai", \
    "human", "kalashtar", "kenku", "minotaur", "mul", "pixie", "revenant", "satyr", "shade", "shardmind", \
    "shifter", "thri-kreen", "tiefling", "vryloka", "warforged", "wilden"]

  ATTRIBUTES = [ "str", "con", "dex", "int", "wis", "cha", "strenght", "constitution", "dexterity", \
    "intelligence", "wisdom", "charisma" ]


end
