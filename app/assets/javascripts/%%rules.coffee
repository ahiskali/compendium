$ ->
  root = exports ? this
  root.rules_to_table = (data) ->
    source = $("#template-rule-row").html()
    template = Handlebars.compile(source, {noEscape: true})
    rules_table = $("#rules")
    rules_table.html ''
    for rule in data
      console.log rule
      rendered = template(rule)
      rules_table.append(rendered)

  root.rules_to_table(gon.rules)
