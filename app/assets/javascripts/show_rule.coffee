$ ->
  root = exports ? this
  root.show_content = (element)->
    element = $(element)
    rule_id = element.attr('id')
    $.ajax
      url: "/rules/#{rule_id}.json"
      dataType: "json"
      error: (jqXHR, textStatus, errorThrown) ->
        console.log "error"
        console.log jqXHR
      success: (data, textStatus, jqXHR) ->
        console.log "success"
        console.log data
        $('#rule_name').text(data.name)
        $('#rule_prereqs').text(data.data.PrintPrereq)
        $('#rule_content').html(data.content)
