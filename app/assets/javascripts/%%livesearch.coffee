$ ->
  root = exports ? this
  $('#search').on 'keyup', ->
    search_params = this.value
    $.ajax
      url: "/rules.json"
      dataType: "json"
      data: {"query": this.value}
      error: (jqXHR, textStatus, errorThrown) ->
        console.log "error"
        console.log jqXHR
      success: (data, textStatus, jqXHR) ->
        console.log "success"
        console.log data
        root.rules_to_table(data)
