class RulesController < ApplicationController
  def index
    @rules = Rule.search(params[:search])
    respond_to do |format|
      format.html { @rules }
      format.json { render json: @rules }
    end
  end

  def show
    @rule = Rule.find(params[:id])
    render json: @rule
  end
end
