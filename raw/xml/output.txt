Found 3702 elements.
{"name"=>"Back to the Wall", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_1", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"11th level; Paragon Tier", "print-prereqs"=>"11th level", "Tier"=>"Paragon", "Short Description"=>"+1 to melee attacks, melee damage, and AC when adjacent to wall", "content"=>"Whenever you are adjacent to a wall, you gain a +1 bonus to melee attack rolls, melee damage rolls, and AC."}

{"name"=>"Blood Thirst", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_2", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"11th level; Paragon Tier", "print-prereqs"=>"11th level", "Tier"=>"Paragon", "Short Description"=>"+2 to melee damage against bloodied enemies", "content"=>"You gain a +2 bonus to melee damage rolls against bloodied foes."}

{"name"=>"Combat Reflexes", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_3", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"Dex 13", "print-prereqs"=>"Dex 13", "Tier"=>"Heroic", "Short Description"=>"+1 to OAs", "content"=>"You gain a +1 bonus to opportunity attack rolls."}

{"name"=>"Defensive Advantage", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_4", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"11th level, Dex 17; Paragon Tier", "print-prereqs"=>"11th level, Dex 17", "Tier"=>"Paragon", "Short Description"=>"+2 to AC against enemies you have CA against", "content"=>"When you have combat advantage against an enemy, you gain a +2 bonus to AC against that enemy's attacks."}

{"name"=>"Fast Runner", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_6", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"Con 13", "print-prereqs"=>"Con 13", "Tier"=>"Heroic", "Short Description"=>"+2 to speed when you charge or run", "content"=>"You gain a +2 bonus to speed when you charge or run."}

{"name"=>"Lost in the Crowd", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_7", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"Halfling", "print-prereqs"=>"Halfling", "Tier"=>"Heroic", "Short Description"=>"+2 to AC when adjacent to 2+ larger enemies", "content"=>"You gain a +2 bonus to AC when you are adjacent to at least two enemies larger than you."}

{"name"=>"Point-Blank Shot", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_8", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"11th level; Paragon Tier", "print-prereqs"=>"11th level", "Tier"=>"Paragon", "Short Description"=>"Ignore cover, superior cover, and concealment within 5 sq. with ranged attacks", "content"=>"If you make a ranged attack against a foe within 5 squares of you, your attack ignores cover and concealment, including superior cover, but not total concealment."}

{"name"=>"Powerful Charge", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_9", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"Str 13", "print-prereqs"=>"Str 13", "Tier"=>"Heroic", "Short Description"=>"On charge, gain +2 to damage and +2 to bull rush", "content"=>"When you charge, you gain a +2 bonus to damage and a +2 bonus to bull rush attempts."}

{"name"=>"Combat Anticipation", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_15", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"11th level; Paragon Tier", "print-prereqs"=>"11th level", "Tier"=>"Paragon", "Short Description"=>"+1 to defenses against ranged, area, and close attacks", "content"=>"You gain a +1 feat bonus to all defenses against ranged, area, and close attacks."}

{"name"=>"Danger Sense", "type"=>"Feat", "internal-id"=>"ID_FMP_FEAT_16", "source"=>"Player's Handbook", "revision-date"=>"8/27/2010 12:17:48 PM", "Prereqs"=>"11th level; Paragon Tier", "print-prereqs"=>"11th level", "Tier"=>"Paragon", "Short Description"=>"Roll twice for initiative", "content"=>"When you make an initiative check, roll twice and take the higher of the two rolls."}

