require 'nokogiri'
require 'rails'
doc = File.open("./combined.xml") { |f| Nokogiri::XML(f) }
doc.remove_namespaces!
res = File.new("./output.txt", "w")
# expertise = doc.xpath("//[@name = 'Flail Expertise']")
# expertise.xpath("ancestor::node()").each {|ancestor| res.puts ancestor; res.puts "###############"}
feats = doc.xpath("//RulesElement[@type = 'Feat']")
res.puts "Found " + feats.size.to_s + " elements."
feats.first(10).each do |feat|
	feat_hash = Hash.new
	feat.attribute_nodes.each do |attribute|
		 feat_hash[attribute.node_name.to_s] = attribute.to_s.strip
	end
	feat.xpath(".//*").each do |specification|
		if specification.attribute("name").blank?
			specification_name = specification.node_name.to_s
		else
			specification_name = specification.attribute("name").to_s
		end
		if !specification.xpath("./text()").to_s.blank? 
			feat_hash[specification_name] = specification.xpath("./text()").to_s.strip
		end
	end
	feat_hash['content'] = feat.xpath("(./text())[normalize-space()]").to_s.strip
	res.puts feat_hash.inspect
	res.puts
	#puts 'content' + ": " + feat.xpath("./text()[normalize-space()]").to_s
	#res.puts feat.attribute("name")
	#res.puts feat.xpath("./print-prereqs/text()")
	#res.puts feat.xpath("(./text())[normalize-space()]")
	#res.puts "|----------------------------------------------------------|"
	#res.puts
end
