require './app/models/prerequisite.rb'
f = File.open("./raw/feat_prereqs/output2.txt", "r")
o = File.new("./raw/feat_prereqs/output_classes.txt", "w")

f.each_line do |line|
  line.split(/\s/).each { |word| o.puts line if Prerequisite::CLASSES.include? word}
end
