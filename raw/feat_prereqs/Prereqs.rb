prereqs_file = File.new("./raw/feat_prereqs/output4.txt", "w")
prereqs = Rule.all.collect { |rule| rule.data["Prereqs"].try(:split, %r{,\s?|;\s?|\sor\s}) }.flatten!.compact!
prereqs = prereqs.inject(Hash.new(0)) { |total, name| total[name] += 1 ;total}
prereqs.sort_by {|_key, value| -value}.to_h.each { |prereq, count| prereqs_file.puts prereq + " " + count.to_s }
