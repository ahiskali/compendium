class CleanPrereq
  def initialize(value)
    @value = value
  end

  def include_power_source?(string)
    if (string.match(/any (.*) class/))
      Prerequisite::POWER_SOURCES.keys.each { |lib_power_source| return lib_power_source if string.match /\b#{lib_power_source}\b/ }
    end
    return nil
  end

  def include_class?(string)
    Prerequisite::CLASSES.each { |character_class| return character_class if string.match /\b#{character_class}\b/ }
    return nil
  end

  def include_race?(string)
    Prerequisite::RACES.each { |race| return race if string.match /\b#{race}\b/ }
    return nil
  end

  def include_attribute?(string)
    Prerequisite::ATTRIBUTES.each { |attribute| return attribute[0..2] if string.match /\b#{attribute}\b/ }
    return nil
  end

  def to_prereq(string, mode = nil)
    disjunction = []
    clean_prereqs = {}
    prereqs = string.split(/, or |\bor\b|,/)
    prereqs.each do |prereq|
      result_hash = Hash.new { |h, k| h[k] = [] }
      if prereq.match /level/
        value = prereq.match(/\d+/).to_s.to_i
        result_hash[:level] = value
      elsif power_source = include_power_source?(prereq)
        result_hash[:power_source] = power_source
      elsif character_class = include_class?(prereq)
        if race = include_race?(prereq)
            result_hash[:race] = race
        end
        result_hash[:class] = character_class
      elsif race = include_race?(prereq)
        result_hash[:race] = race
      elsif attribute = include_attribute?(prereq)
        value = prereq.match(/\d+/).to_s.to_i
        result_hash[attribute] = value
      end
      if mode == :disjunction
        disjunction << result_hash
      else
        clean_prereqs.merge! result_hash
      end
    end
    return mode == :disjunction ? disjunction : clean_prereqs
  end

  def clean
    return unless @value
    @value.downcase!
    clean_prereqs = []
    prereqs_hash = {}
    @value.split(";").compact.each do |prereq|
      if prereq.match /\bor\b/
        prereqs = to_prereq(prereq, :disjunction)
        clean_prereqs << prereqs
      else
        prereqs = to_prereq(prereq)
        prereqs_hash.merge! prereqs #{ |key, oldval, newval| oldval | newval }
      end
    end
    clean_prereqs.unshift prereqs_hash unless prereqs_hash.empty?
    return clean_prereqs
  end

end
