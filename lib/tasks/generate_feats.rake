namespace :generate_feats do
  def split_prereqs(value)
    return unless value
    value.split(%r{,\s?|;\s?|\sor\s}).flatten.compact
  end

  task :generate => :environment do
    Rule.destroy_all
    Prerequisite.destroy_all

    path = Rails.root.join("vendor/combined.xml")
    doc = File.open(path) { |f| Nokogiri::XML(f) }
    doc.remove_namespaces!
    # expertise = doc.xpath("//[@name = 'Flail Expertise']")
    # expertise.xpath("ancestor::node()").each {|ancestor| res.puts ancestor; res.puts "###############"}
    feats = doc.xpath("//RulesElement[@type = 'Feat']")
    feats.each do |feat|
      specification_hash = {}

      feat.xpath(".//*").each do |specification|
    		if specification.attribute("name").blank?
    			specification_name = specification.node_name.to_s.strip.tr(' ', '_').camelize.underscore.classify
    		else
    			specification_name = specification.attribute("name").to_s.strip.tr(' ', '_').camelize.underscore.classify
    		end
    		if specification.xpath("./text()").to_s.present?
    			specification_hash[specification_name] = specification.xpath("./text()").to_s.strip
    		end
    	end

      internal_id = feat.attribute('internal-id')
      rule = Rule.new(internal_id: internal_id)
    	rule.type = feat.attribute('type').to_s.strip.tr(' ', '_').camelize.underscore.classify
    	rule.name = feat.attribute('name').to_s.strip
      rule.source = feat.attribute('source').to_s.strip
      rule.data = specification_hash
      prerequisites = split_prereqs(specification_hash['PrintPrereq'])
      prerequisites_models = prerequisites && prerequisites.map do |pre|
        Prerequisite.find_or_create_by!(name: pre)
      end
      rule.prerequisites = prerequisites_models if prerequisites_models.present?
    	rule.content = feat.xpath("(./text())[normalize-space()]").to_s.strip.gsub(/\n/, '<br><br>')
    	rule.save!
    end
    	#puts 'content' + ": " + feat.xpath("./text()[normalize-space()]").to_s
    	#res.puts feat.attribute("name")
    	#res.puts feat.xpath("./print-prereqs/text()")
    	#res.puts feat.xpath("(./text())[normalize-space()]")
    	#res.puts "|----------------------------------------------------------|"
    	#res.puts
  end
end
