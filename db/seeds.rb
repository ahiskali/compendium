# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'nokogiri'
require 'rails'
task :RulesElement => :environment do
doc = File.open("./combined.xml") { |f| Nokogiri::XML(f) }
doc.remove_namespaces!
# res = File.new("./output.txt", "w")
# expertise = doc.xpath("//[@name = 'Flail Expertise']")
# expertise.xpath("ancestor::node()").each {|ancestor| res.puts ancestor; res.puts "###############"}
feats = doc.xpath("//RulesElement[@type = 'Feat']")
feats.first(10).each do |feat|
  # res.puts "HEYOO"
	feat_hash = Hash.new
	feat.attribute_nodes.each do |attribute|
		 feat_hash[attribute.node_name.to_s] = attribute.to_s.strip
	end
	feat.xpath(".//*").each do |specification|
		if specification.attribute("name").blank?
			specification_name = specification.node_name.to_s
		else
			specification_name = specification.attribute("name").to_s
		end
		if !specification.xpath("./text()").to_s.blank?
			feat_hash[specification_name] = specification.xpath("./text()").to_s.strip
		end
	end
	feat_hash['content'] = feat.xpath("(./text())[normalize-space()]").to_s.strip
  Rule.create(feat_hash)
  end
	#puts 'content' + ": " + feat.xpath("./text()[normalize-space()]").to_s
	#res.puts feat.attribute("name")
	#res.puts feat.xpath("./print-prereqs/text()")
	#res.puts feat.xpath("(./text())[normalize-space()]")
	#res.puts "|----------------------------------------------------------|"
	#res.puts
end
