require 'nokogiri'
doc = File.open("D:/ahiskali/Projects/Code/Ruby/xml/combined.xml") { |f| Nokogiri::XML(f) }
doc.remove_namespaces!
res = File.new("D:/ahiskali/Projects/Code/Ruby/xml/output.txt", "w")
# expertise = doc.xpath("//*[@name = 'Flail Expertise']")
# expertise.xpath("ancestor::node()").each {|ancestor| res.puts ancestor; res.puts "###############"}
feats = doc.xpath("//*[@type = 'Feat' and contains(.,'prone')]")
res.puts "Found " + feats.size.to_s + " elements."
feats.each do |feat|
	res.puts feat.attribute("name")
	res.puts feat.xpath("./print-prereqs/text()")
	res.puts feat.xpath("(./text())[normalize-space()]")
	res.puts "|----------------------------------------------------------|"
	res.puts
end