class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.string :name
      t.string :type
      t.string :internal_id
      t.string :source
      t.text :content
      t.jsonb :data

      t.timestamps null: false
    end
  end
end
