class CreatePrerequisites < ActiveRecord::Migration
  def change
    create_table :prerequisites do |t|
      t.string :name
      t.integer :value, default: 1

      t.timestamps null: false
    end

    add_index :prerequisites, [:name, :value], unique: true

    create_table :prerequisites_rules, id: false do |t|
      t.integer :rule_id
      t.integer :prerequisite_id
    end
  end
end
